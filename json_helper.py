import json

# Loads json into an object, returns the object
def load_json():
    with open('channels.json') as f:
        return json.load(f)


# Write given data to the json file
def write_json(data):
    with open('channels.json', 'w') as outfile:
        json.dump(data, outfile, indent=4)


# Checks if user already has a channel. Returns true if he does, false if he doesn't
def user_has_channel(server_id, user_id):
    data = load_json()
    for channel in data['channels']:
        if channel['user_id'] == user_id:
            return True
    return False


# Adds a new entry to the json
def add_channel(server_id, channel_id, user_id):
    data = load_json()
    channel = {"server_id": server_id, "channel_id": channel_id, "user_id": user_id}
    data['channels'].append(channel)
    write_json(data)


# remove entry
def remove_channel(server_id, user_id):
    data = load_json()
    obj = None
    for i in range(len(data['channels'])):
        if data['channels'][i]['user_id'] == user_id and data['channels'][i]['server_id'] == server_id:
            obj = data['channels'][i]
            data['channels'].pop(i)
    write_json(data)
    return obj